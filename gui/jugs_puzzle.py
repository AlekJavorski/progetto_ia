# author ad71
import os.path
import random
import sys
import time
from functools import partial
from tkinter import *

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from search import astar_search, EightPuzzle, breadth_first_tree_search, JugsPuzzle

root = Tk()

state = (0, 0)
puzzle = JugsPuzzle(tuple(state))
solution = None


def solve():
    """ Solves the puzzle using astar_search """

    return breadth_first_tree_search(puzzle).solution()


def solve_steps():
    """ Solves the puzzle step by step """

    global puzzle
    global solution
    global state
    solution = solve()
    print(solution)


def init():
    """ Calls necessary functions """

    global state
    global solution
    state = (0, 0)
    solve_steps()


init()

