# author ad71 & Aleksander Jaworski
import os.path
import random
import sys
import time
from functools import partial
from tkinter import *

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from search import astar_search, DoorsPuzzle, DoorProblemState, breadth_first_tree_search, best_first_graph_search, depth_limited_search, iterative_deepening_search

root = Tk()

sample_initial = DoorProblemState(
        map=['_', 'A', 'keyU', None, 'item553', 'D', 'keyA'],
        doors_keys={'A': ['keyA', 'keyU'], 'D': ['keyU']},
        door_states={'A': 'open', 'D': 'closed'},
        position=4)

sample_goal = DoorProblemState(
        map=['_', 'A', 'keyU', None, 'item553', 'D', 'keyA'],
        doors_keys={'A': ['keyA', 'keyU'], 'D': ['keyU']},
        door_states={'A': 'open', 'D': 'open'},
        position=0)

puzzle = DoorsPuzzle(sample_initial, sample_goal)
solution = None


def solve():
    """ Solves the puzzle using astar_search """

    return iterative_deepening_search(puzzle).solution()


def solve_steps():
    """ Solves the puzzle step by step """

    global puzzle
    global solution
    global state
    solution = solve()
    print(solution)


def init():
    """ Calls necessary functions """

    global state
    global solution
    state = DoorsPuzzle.sample_initial
    solve_steps()


init()

